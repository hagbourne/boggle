
import java.awt.*;
import java.awt.event.*;
import java.util.TreeSet;

import javax.swing.*;

/**
 * User interface for the Boggle game objects, DiceTray, WordTree and Scorer.
 * <p>
 * Since I've never used AWT or Swing before and since this is the last project
 * I've had a bit of a play. I have used a JLayeredPane to provide three layers.
 * The lowest layer is simply a background image, the middle layer has all the
 * components including a JPanel for the dice grid and the top layer for the
 * popup score display.
 *
 * @author Keith Talbot
 * @version 12/3/2007
 */
public class BoggleFrame extends JFrame implements ActionListener {

    /**
     * Application launcher.
     *
     * @param args
     */
    public static void main(String[] args) {
        BoggleFrame boggleFrame = new BoggleFrame();
    }

    /**
     * Constructor to initialize the user interface and then start the
     * application.
     */
    public BoggleFrame() {
        this.initialize();
        this.start();
    }

    /*
	 * User interface components.
     */
    private JLayeredPane mainLayeredPane;
    private JLabel backgroundLabel;
    private JPanel gridPanel;
    private JLabel[] gridLabelArray;
    private JScrollPane wordsScrollPane;
    private JTextArea wordsTextArea;
    private JButton mainButton;
    private JScrollPane scoreScrollPane;
    private JTextPane scoreTextPane;

    /*
	 * Application objects.
     */
    private DiceTray boggleDiceTray;
    private WordTree boggleWordTree;
    private Scorer boggleScorer;

    private int buttonState;

    /**
     * Initialize the frame, all user interface components, register listeners
     * and show the window.
     */
    private void initialize() {
        // Frame
        this.setTitle("Boggle for CS227");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLocation(50, 50);
        this.setResizable(false);

        // Layers, 10 = background, 20 = main components, 30 = score pane
        mainLayeredPane = new JLayeredPane();
        mainLayeredPane.setPreferredSize(new Dimension(300, 600));

        // Background image label
        backgroundLabel = new JLabel(new ImageIcon("res\\background.jpg"));
        backgroundLabel.setBounds(0, 0, 300, 600);
        mainLayeredPane.add(backgroundLabel, 10, 0);

        // Grid of labels (4x4)
        gridPanel = new JPanel();
        gridPanel.setLayout(new GridLayout(4, 4));
        gridPanel.setBounds(5, 85, 290, 300);
        gridPanel.setOpaque(false);

        gridLabelArray = new JLabel[16];
        for (int i = 0; i < 16; i++) {
            gridLabelArray[i] = new JLabel("", JLabel.CENTER);
            gridLabelArray[i].setFont(new Font("Arial", Font.BOLD, 36));
            gridLabelArray[i].setForeground(new Color(0.0F, 0.0F, 0.33F));
            gridPanel.add(gridLabelArray[i]);
        }

        mainLayeredPane.add(gridPanel, 20, 0);

        // Scrollable text area for word entry
        wordsTextArea = new JTextArea();
        wordsTextArea.setFont(new Font("Arial", Font.PLAIN, 16));
        wordsTextArea.setEditable(false);
        wordsTextArea.setBackground(Color.LIGHT_GRAY);
        wordsScrollPane = new JScrollPane();
        wordsScrollPane.setBounds(15, 400, 270, 150);
        wordsScrollPane.setViewportView(wordsTextArea);
        mainLayeredPane.add(wordsScrollPane, 20, 0);

        // Single button (Start -> Stop -> Show Score -> Show All -> Reset ->
        // Start..)
        mainButton = new JButton("Start");
        mainButton.setBounds(90, 555, 120, 33);
        mainLayeredPane.add(mainButton, 20, 0);

        // Popup score text pane (hidden).
        scoreTextPane = new JTextPane();
        scoreTextPane.setContentType("text/html");
        scoreTextPane.setEditable(false);
        scoreTextPane.setBackground(new Color(1.0F, 1.0F, 0.6F));
        scoreScrollPane = new JScrollPane();
        scoreScrollPane.setBounds(25, 25, 250, 515);
        scoreScrollPane.setViewportView(scoreTextPane);
        scoreScrollPane.setVisible(false);
        mainLayeredPane.add(scoreScrollPane, 30, 0);

        this.getContentPane().add(mainLayeredPane);

        // Listeners
        mainButton.addActionListener(this);

        this.pack();
        this.setVisible(true);
    }

    /**
     * Instantiate the boggle objects to start the application.
     */
    private void start() {
        boggleDiceTray = new DiceTray();
        boggleWordTree = new WordTree("res\\bogglewords.txt");
        boggleScorer = new Scorer();
    }

    // Single button (Start -> Stop -> Show Score -> Show All -> Reset ->
    // Start..)
    /**
     * The event listener for the single control button.
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        /*
		 * The control button cycles between the following states: Start -> Stop ->
		 * Show Score -> Show All -> Reset -> Start..
         */
        switch (buttonState) {
            case 0:
                // Start, show a new dice grid and allow entry of words by the user.
                mainButton.setText("Stop");
                wordsTextArea.setEditable(true);
                wordsTextArea.setBackground(Color.WHITE);
                boggleDiceTray.shake();
                setGrid(boggleDiceTray.getLetters());
                break;
            case 1:
                // Stop, stop entry of words and convert entered text to upper case
                // to simplify checking.
                mainButton.setText("Show Score");
                wordsTextArea.setText(wordsTextArea.getText().toUpperCase());
                wordsTextArea.setEditable(false);
                wordsTextArea.setBackground(Color.LIGHT_GRAY);
                break;
            case 2:
                // Show Score, calculate the user's score and display it in the
                // popup score panel.
                mainButton.setText("Show All");
                boggleScorer.score(wordsTextArea.getText(), boggleDiceTray, boggleWordTree);
                scoreTextPane.setText(boggleScorer.getHtml("Your Word Score"));
                scoreScrollPane.setVisible(true);
                break;
            case 3:
                // Show All, find all possible words, calculate the possible score
                // and display it in the popup score panel.
                mainButton.setText("Reset");
                TreeSet<String> allWords = boggleDiceTray.findAllWords(boggleWordTree);
                boggleScorer.score(allWords);
                scoreTextPane.setText(boggleScorer.getHtml("Possible Word Score"));
                break;
            case 4:
                // Reset, clear the user interface ready for the next game.
                mainButton.setText("Start");
                wordsTextArea.setText("");
                scoreScrollPane.setVisible(false);
                resetGrid();
                break;
        }

        buttonState = ++buttonState % 5;
    }

    /**
     * Set the letter grid from a string of 16 letters.
     *
     * @param letters A srtign of 16 letters.
     */
    private void setGrid(String letters) {
        for (int i = 0; i < 16; i++) {
            gridLabelArray[i].setText(letters.substring(i, i + 1));
        }
    }

    /**
     * Set the letter grid to 16 blank letters.
     */
    private void resetGrid() {
        for (int i = 0; i < 16; i++) {
            gridLabelArray[i].setText("");
        }
    }
}
