
public class BoggleMain {

    /**
     * Required class to launch the application. (For 2 points!)
     *
     * @param args Arguments.
     */
    public static void main(String[] args) {
        BoggleFrame.main(args);
    }
}
