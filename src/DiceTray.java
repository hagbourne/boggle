
import java.util.Random;
import java.util.TreeSet;

/**
 * A Boggle DiceTray which holds a tray of 16 dice which can be shaken and
 * searched.
 *
 * @author Keith Talbot
 * @version 11/27/2007
 *
 */
public class DiceTray {

    private Dice[][] tray;

    /**
     * Constructor which creates a full DiceTray. It uses a set of internal dice
     * which match the actual game dice. The dice are shaken on instantiation.
     */
    public DiceTray() {
        tray = new Dice[4][4];

        tray[0][0] = new Dice("LRYTTE");
        tray[0][1] = new Dice("VTHRWE");
        tray[0][2] = new Dice("EGHWNE");
        tray[0][3] = new Dice("SEOTIS");
        tray[1][0] = new Dice("ANAEEG");
        tray[1][1] = new Dice("IDSYTT");
        tray[1][2] = new Dice("OATTOW");
        tray[1][3] = new Dice("MTOICU");
        tray[2][0] = new Dice("AFPKFS");
        tray[2][1] = new Dice("XLDERI");
        tray[2][2] = new Dice("HCPOAS");
        tray[2][3] = new Dice("ENSIEU");
        tray[3][0] = new Dice("YLDEVR");
        tray[3][1] = new Dice("ZNRNHL");
        tray[3][2] = new Dice("NMIQHU");
        tray[3][3] = new Dice("OBBAOJ");
    }

    /**
     * Constructor which creates a full DiceTray with preset dice given. This
     * constructor is used for testing only.
     *
     * @param letterArray A two dimensional array of char representing the
     * preset dice positions.
     */
    public DiceTray(char[][] letterArray) {
        tray = new Dice[4][4];

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                tray[i][j] = new Dice(letterArray[i][j]);
            }
        }

    }

    /**
     * Return the 16 dice letters as a single string. This is used to simplify
     * insertion into the user interface.
     *
     * @return The 16 dice letters as a String of letters.
     */
    public String getLetters() {
        StringBuilder str = new StringBuilder();

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                str.append(tray[i][j].letter);
            }
        }

        return str.toString();
    }

    /**
     * Shake the dice by removing and replacing randomly all of the Dice objects
     * while running the spin() method of each.
     */
    public void shake() {
        Dice[] rolling = new Dice[16];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                rolling[(i * 4) + j] = tray[i][j];
            }
        }

        Random generator = new Random();

        int dice = 16;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                int rnd = generator.nextInt(dice);
                tray[i][j] = rolling[rnd];
                tray[i][j].spin();

                dice--;

                for (int k = rnd; k < dice; k++) {
                    rolling[k] = rolling[k + 1];
                }
            }
        }
    }

    /**
     * Determine if a search string is found within the DiceTray.
     *
     * @param search String to search for.
     * @return True if the search string is found within the DiceTray, false
     * otherwise.
     */
    public boolean stringFound(String search) {
        if (search == null || search.length() < 1) {
            return false;
        }

        // Search for the string recursively, starting from every dice position.
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                tray[i][j].checking = true;
                boolean ret = stringFound(search, i, j);
                tray[i][j].checking = false;

                if (ret) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Recursive method of stringFound to determine if a search string is found
     * within the DiceTray.
     *
     * @param search String to search for.
     * @param row
     * @param col
     * @return True if the search string is found within the DiceTray, false
     * otherwise.
     */
    private boolean stringFound(String search, int row, int col) {
        /*
		 * If the current letter does not match, backtrack. If it matches and is
		 * the last letter, return true all the way up the stack. Otherwise
		 * search for the next letter on any remaining surrounding dice.
         */
        if (search.charAt(0) != tray[row][col].letter) {
            return false;
        } else if (search.length() <= 1) {
            return true;
        } else {
            for (int i = -1; i < 2; i++) {
                for (int j = -1; j < 2; j++) {
                    int r = row + i;
                    int c = col + j;

                    // Ensure it is both a valid die and also not already
                    // included in the found string.
                    if (r >= 0 && r < 4 && c >= 0 && c < 4 && !tray[r][c].checking) {

                        tray[r][c].checking = true;
                        boolean ret = stringFound(search.substring(1, search.length()), r, c);
                        tray[r][c].checking = false;

                        // Return true all the way up the stack.
                        if (ret) {
                            return ret;
                        }
                    }
                }
            }

            return false;
        }
    }

    /**
     * Find all of the valid words in the current tray by comparing all possible
     * letter combinations with a given WordTree with all valid dictionary
     * words.
     * <p>
     * The algorithm works recursively in a similar fashion to the stringFound
     * method except that rather than stopping when a word is matched against a
     * target string it continues searching until all combinations are tried.
     * <p>
     * To prevent continuing when it is impossible to begin a word with the
     * letters so far, a base case exists when the WordTree returns a zero
     * result which indicates that no word stems exist for the current string.
     *
     * @param allWords A WordTree with all valid dictionary words.
     * @return A TreeSet object with all of the found words.
     */
    public TreeSet<String> findAllWords(WordTree allWords) {
        TreeSet<String> foundWords = new TreeSet<>();

        // Find all words recursively, starting from every dice position.
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                tray[i][j].checking = true;
                findAllWords(i, j, "", foundWords, allWords);
                tray[i][j].checking = false;
            }
        }

        return foundWords;
    }

    /**
     * Recursive method of stringFound to find all of the valid words in the
     * current tray
     *
     * @param search String to search for.
     * @param row
     * @param col
     * @return True if the search string is found within the DiceTray, false
     * otherwise.
     */
    /**
     * Recursive method of findAllWords to find all of the valid words in the
     * current tray. within the DiceTray.
     *
     * @param row Current grid row.
     * @param col Current grid column.
     * @param wordStem The letters used so far as a String.
     * @param foundWords A TreeSet of all words found so far.
     * @param allWords A WordTree with all valid dictionary words.
     */
    private void findAllWords(int row, int col, String wordStem, TreeSet<String> foundWords, WordTree allWords) {
        String word = wordStem + tray[row][col].letter;
        int foundFlag = allWords.findWord(word);

        if (foundFlag == 0) {
        } else {
            if (foundFlag > 0) {
                foundWords.add(word);
            }

            for (int i = -1; i < 2; i++) {
                for (int j = -1; j < 2; j++) {
                    int r = row + i;
                    int c = col + j;

                    if (r >= 0 && r < 4 && c >= 0 && c < 4 && !tray[r][c].checking) {
                        tray[r][c].checking = true;
                        findAllWords(r, c, word, foundWords, allWords);
                        tray[r][c].checking = false;
                    }
                }
            }

        }
    }

    /**
     * Overridden toString implementation to show both the game dice and also
     * the current search path.
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                str.append(tray[i][j].letter);
                if (tray[i][j].checking) {
                    str.append("* ");
                } else {
                    str.append("  ");
                }
            }
            str.append("\n");
        }

        return str.toString();
    }

    /**
     * Internal class used to represent a single die, its current letter, all
     * its faces and whether it is being included as part of a search string.
     *
     * @author talbotk
     *
     */
    private class Dice {

        public char letter;
        public char[] faces;
        public boolean checking;

        /**
         * Constructor which creates a die with a single letter. This
         * constructor is used for testing only.
         *
         * @param testLetter Single letter for the entire die.
         */
        public Dice(char testLetter) {
            faces = null;
            letter = testLetter;
            checking = false;
        }

        /**
         * Constructor which creates a die with the faces set to the given
         * letters. This is the real game die which can then be spun.
         *
         * @param letters Six letters in an String for the sides of the die.
         */
        public Dice(String letters) {
            if (letters != null) {
                faces = new char[letters.length()];
                for (int i = 0; i < letters.length(); i++) {
                    faces[i] = letters.charAt(i);
                }
                letter = faces[0];
                checking = false;
            } else {
                faces = null;
                letter = '#';
                checking = false;
            }
        }

        /**
         * Spin a die by selecting on of it's faces randnmly.
         */
        public void spin() {
            Random generator = new Random();
            letter = faces[generator.nextInt(faces.length)];
        }
    }
}
