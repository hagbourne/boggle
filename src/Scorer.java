
import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeSet;

/**
 * A class to calculate the score of a given DiceTray, checked against a given
 * WordTree. The words and score are saved internally and then returned for
 * display when requested.
 *
 * @author Keith Talbot
 * @version 12/3/2007
 */
public class Scorer {

    private ArrayList<Word> words;
    private int totalScore;

    /**
     * Accessor for the current total score.
     *
     * @return The total score.
     */
    public int getTotal() {
        return totalScore;
    }

    /**
     * Return the current words, word score, total and the given heading as an
     * HTML fragment in a table.
     *
     * @param heading A string to be added as the heading.
     * @return An HTML fragment as a string.
     */
    public String getHtml(String heading) {
        StringBuilder str = new StringBuilder();

        str.append("<style>\n");
        str.append("div {padding: 10px;}");
        str.append("table {font:normal normal 12pt Arial;}");
        str.append("th {background:#ffffdd;}");
        str.append("</style>\n");

        str.append("<div>\n");
        str.append("<h3>").append(heading).append("</h3>\n");
        str.append("<table width='100%' cellpadding='0' cellspacing='1'>\n");
        str.append(String.format("<tr><th>Word</th> <th>Score</th></tr>\n", totalScore));

        for (Word w : words) {
            str.append(String.format("<tr><td>%1$s</td> <td>%2$d</td>\n", w.word, w.score));
        }

        str.append(String.format("<tr><th>Total</th> <th>%1$d</th></tr>\n", totalScore));
        str.append("</table>\n");
        str.append("</div>\n");

        return str.toString();
    }

    /**
     * Calculate the score of the given words, in the given DiceTray and in the
     * given WordTree.
     * <p>
     * This uses an alternate method for the calculation which requires the
     * words in a TreeSet.
     *
     * @param wordString Words to be scored.
     * @param diceTray DiceTray to be searched.
     * @param allWords WordTree to be searched
     */
    public void score(String wordString, DiceTray diceTray, WordTree allWords) {
        TreeSet<String> trialWords = new TreeSet<>();
        Scanner wordScanner = new Scanner(wordString);

        while (wordScanner.hasNext()) {
            trialWords.add(wordScanner.next());
        }

        score(trialWords, diceTray, allWords);
    }

    /**
     * Calculate the score of the given words that are assumed to already be in
     * the DiceTray and in the WordTree.
     * <p>
     * This uses an alternate method for the calculation but passes in a null
     * DiceTray and WordTree.
     *
     * @param trialWords TreeSet of words to be scored.
     */
    public void score(TreeSet<String> trialWords) {
        score(trialWords, null, null);
    }

    /**
     * Calculate the score of the given words, in the given DiceTray and in the
     * given WordTree.
     *
     * @param trialWords TreeSet of words to be scored.
     * @param diceTray DiceTray to be searched.
     * @param allWords WordTree to be searched
     */
    public void score(TreeSet<String> trialWords, DiceTray diceTray, WordTree allWords) {
        words = new ArrayList<>();
        totalScore = 0;

        for (String thisWord : trialWords) {
            Word newWord = new Word(thisWord);

            if (thisWord.length() >= 8) {
                newWord.score = 11;
            } else if (thisWord.length() >= 7) {
                newWord.score = 5;
            } else if (thisWord.length() >= 6) {
                newWord.score = 3;
            } else if (thisWord.length() >= 5) {
                newWord.score = 2;
            } else if (thisWord.length() >= 3) {
                newWord.score = 1;
            }

            if (diceTray != null && allWords != null) {
                if (diceTray.stringFound(thisWord) && allWords.findWord(thisWord) > 0) {
                    newWord.isValid = true;
                } else {
                    newWord.isValid = false;
                    newWord.score = newWord.score * (-1);
                }
            } else {
                newWord.isValid = true;
            }

            words.add(newWord);
            totalScore += newWord.score;
        }
    }

    /**
     * Overridden toString implementation to show the current score.
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();

        for (Word w : words) {
            str.append(String.format("%1$s (%2$d)\n", w.word, w.score));
        }

        str.append("\n");
        str.append(String.format("Total: %1$d\n", totalScore));

        return str.toString();
    }

    /**
     * Internal class to represent a single word with its score and whether it
     * is a valid word.
     *
     * @author Keith Talbot
     * @version 12/3/2007
     */
    private class Word {

        public String word;
        public boolean isValid;
        public int score;

        public Word(String newWord) {
            word = newWord;
        }
    }
}
