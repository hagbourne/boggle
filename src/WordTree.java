
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * A tree structure to store a dictionary of words which can be searched to find
 * both valid words, word stems and non-word stems.
 * <p>
 * If a binary tree has up to 2 children per node then this tree with up to 26
 * children per node must be a sexvigenary tree.
 * <p>
 * There is a node per character of the English alphabet. The only data stored
 * is a boolean which indicates if the nodes in the tree to this point is a
 * complete word.
 *
 * @author Keith Talbot
 * @version 11/30/2007
 *
 */
public final class WordTree {

    private AlphaNode root;

    /**
     * Constructor which builds a tree of words from a given file of words.
     *
     * @param fileName A file of words.
     */
    public WordTree(String fileName) {
        try {
            Scanner inFile = new Scanner(new File(fileName));

            root = new AlphaNode();
            while (inFile.hasNextLine()) {
                addWord(root, inFile.nextLine().toLowerCase());
            }

            inFile.close();
        } catch (FileNotFoundException e) {
            System.out.println("Error reading from file: " + fileName);
        }
    }

    /**
     * Recursively add a single word to the tree.
     *
     * @param node Current tree node.
     * @param word Remainder of word to be added.
     */
    private void addWord(AlphaNode node, String word) {
        if (word.length() == 0) {
            node.isWord = true;
        } else {
            AlphaNode next = node.setNode(word.charAt(0));
            addWord(next, word.substring(1, word.length()));
        }
    }

    /**
     * Find a possible word in the tree.
     *
     * @param word Word to find.
     * @return An integer which is either the positive length for a complete
     * word, the negative length for a word stem only and zero if not a viable
     * word or word stem.
     */
    public int findWord(String word) {
        return findWord(root, word.toLowerCase(), 0);
    }

    /**
     * Recursive helper function for findWord.
     *
     * @param node Current node being searched.
     * @param word Word to find.
     * @param length Length found so far.
     * @return An integer which is either the positive length for a complete
     * word, the negative length for a word stem only and zero if not a viable
     * word or word stem.
     */
    private int findWord(AlphaNode node, String word, int length) {
        if (word.length() == 0) {
            // There is nothing left to find, return positive length if this
            // node
            // is the end of a complete word or the negative length if just a
            // word
            // stem.
            if (node.isWord) {
                return length;
            } else {
                return (-1) * length;
            }
        } else {
            // There is more to find, return null if there was no node for this
            // letter or recurse to the next letter.
            AlphaNode next = node.getNode(word.charAt(0));
            if (next == null) {
                return 0;
            } else {
                return findWord(next, word.substring(1, word.length()), length + 1);
            }
        }
    }

    /**
     * A tree node with up to 26 child nodes representing the letters of the
     * English alphabet.
     *
     * @author Keith Talbot
     * @version 11/30/2007
     */
    private class AlphaNode {

        public AlphaNode[] alphaNodes;
        public boolean isWord;

        /**
         * Constructor.
         */
        public AlphaNode() {
            alphaNodes = new AlphaNode[26];
        }

        /**
         * Set the node for a given character.
         *
         * @param alpha Character to set a new node for.
         * @return The new (or existing) node.
         */
        public AlphaNode setNode(char alpha) {
            int i = Character.digit(alpha, 36) - 10;

            if (0 <= i && i <= 26) {
                if (alphaNodes[i] == null) {
                    alphaNodes[i] = new AlphaNode();
                }
                return alphaNodes[i];
            } else {
                return null;
            }

        }

        /**
         * Get the node for a given character.
         *
         * @param alpha Character to get a new node for.
         * @return Node or null.
         */
        public AlphaNode getNode(char alpha) {
            int i = Character.digit(alpha, 36) - 10;

            if (0 <= i && i <= 26 && alphaNodes[i] != null) {
                return alphaNodes[i];
            } else {
                return null;
            }
        }
    }

}
