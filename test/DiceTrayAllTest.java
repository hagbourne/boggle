
import static org.junit.Assert.*;
import java.util.TreeSet;
import org.junit.Test;

/**
 * Testing of the DiceTray class methods for part 2 of the project.
 *
 * @author Keith Talbot
 * @version 12/04/2007
 */
public class DiceTrayAllTest {
    //
    // Unshaken, the DiceTray constructor will always contain these dice.
    //
    // L  V  E  S  
    // A  I  O  M  
    // A  X  H  E  
    // Y  Z  N  O  
    //

    /**
     *
     */
    @Test
    public void findAllTest() {
        DiceTray dt = new DiceTray();
        System.out.println(dt.toString());

        WordTree wt = new WordTree("res\\bogglewords.txt");

        TreeSet<String> result = dt.findAllWords(wt);

        assertEquals(60, result.size());
        assertTrue(result.contains("MOVIES"));
        assertTrue(result.contains("SEMEN"));
        assertFalse(result.contains("MONO"));
        assertFalse(result.contains("SOY"));

//		for (String str : result) {
//			System.out.println(str);
//		}
    }

    /**
     *
     */
    @Test
    public void shakeTest() {
        DiceTray dt = new DiceTray();
        System.out.println(dt.toString());

        dt.shake();
        System.out.println(dt.toString());

        dt.shake();
        System.out.println(dt.toString());
    }

    /**
     *
     */
    @Test
    public void lettersTest() {
        DiceTray dt = new DiceTray();
        assertEquals("LVESAIOMAXHEYZNO", dt.getLetters());
    }
}
