
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Testing of the DiceTray class.
 *
 * @author Keith Talbot
 * @version 11/27/2007
 */
public class DiceTrayTest {

    private final char[][] azTray = {{'A', 'B', 'C', 'D'}, {'E', 'F', 'G', 'H'},
    {'I', 'J', 'K', 'L'}, {'M', 'N', 'O', 'P'}};

    /**
     * Find a variety strings starting from all 16 dice.
     */
    @Test
    public void successTest() {
        DiceTray dt = new DiceTray(azTray);
        System.out.println(dt.toString());
        assertTrue(dt.stringFound("AFKLHGJM"));
        assertTrue(dt.stringFound("BAEIMNKG"));
        assertTrue(dt.stringFound("CDHGBFJN"));
        assertTrue(dt.stringFound("DGFEIJKN"));
        assertTrue(dt.stringFound("EFGKONMIJ"));
        assertTrue(dt.stringFound("FEABGJMIN"));
        assertTrue(dt.stringFound("GLOJEBCHK"));
        assertTrue(dt.stringFound("HLPONMIEA"));
        assertTrue(dt.stringFound("IFCBAEJNM"));
        assertTrue(dt.stringFound("JONMIFKLP"));
        assertTrue(dt.stringFound("KGCBAFJNOP"));
        assertTrue(dt.stringFound("LGBAFKPOJ"));
        assertTrue(dt.stringFound("MIJNOKLP"));
        assertTrue(dt.stringFound("NIEABCGKJM"));
        assertTrue(dt.stringFound("O"));
        assertTrue(dt.stringFound("POJMNIEBAFGKLHDC"));
    }

    /**
     * Fail to find any of these strings.
     */
    @Test
    public void failTest() {
        DiceTray dt = new DiceTray(azTray);
        assertFalse(dt.stringFound(""));
        assertFalse(dt.stringFound("Q"));
        assertFalse(dt.stringFound("AC"));
        assertFalse(dt.stringFound("PM"));
        assertFalse(dt.stringFound("FKGJF"));
        assertFalse(dt.stringFound("NOPJ"));
        assertFalse(dt.stringFound("ABCZ"));
        assertFalse(dt.stringFound("ABCDHGFEIJKLPOMN"));
    }

    private final char[][] abTray = {{'A', 'B', 'A', 'B'}, {'B', 'A', 'B', 'A'},
    {'A', 'B', 'A', 'B'}, {'B', 'A', 'B', 'A'}};

    /**
     * Find strings in a tray full of duplicates. I don't know how valid this
     * test really is but it might find some odd special cases.
     */
    @Test
    public void alternateTest() {
        DiceTray dt = new DiceTray(abTray);
        assertTrue(dt.stringFound("A"));
        assertTrue(dt.stringFound("ABABAB"));
        assertTrue(dt.stringFound("AAAAAAA"));
        assertFalse(dt.stringFound("AAAAAAAA"));
    }

    /**
     * Rick's find tests.
     */
    @Test
    public void testStringFindWhenThereStartingInUpperLeftCorner() {
        DiceTray dt = new DiceTray(azTray);
        assertTrue(dt.stringFound("ABC"));
        assertTrue(dt.stringFound("ABF"));
        assertTrue(dt.stringFound("ABC"));
        assertTrue(dt.stringFound("ABCD"));
        assertTrue(dt.stringFound("ABFEJINM"));
        assertTrue(dt.stringFound("ABEFIJMN"));
        assertTrue(dt.stringFound("ABCDHGFEIJKLPONM"));
        assertTrue(dt.stringFound("ABCDHLPOKJNMIEFG"));
    }

    /**
     * Rick's fail tests.
     */
    @Test
    public void testStringFindWhenNotThere() {
        DiceTray dt = new DiceTray(azTray);
        assertFalse(dt.stringFound("ACB"));
        assertFalse(dt.stringFound("AIE"));
        assertFalse(dt.stringFound("AKF"));
    }

    /**
     * Rick's fail on duplicate tests.
     */
    @Test
    public void testStringFindWhenAttemptIsMadeToUseALetterTwice() {
        DiceTray dt = new DiceTray(azTray);
        assertFalse(dt.stringFound("ABA"));
        assertFalse(dt.stringFound("AFA"));
    }

    private final char[][] reTray = {{'R', 'E', 'D', 'M'}, {'B', 'A', 'N', 'O'},
    {'T', 'Q', 'D', 'F'}, {'L', 'O', 'E', 'V'}};

    /**
     * Special test to investigate this issue returned from Web-CAT.
     *
     * "Could not find 'DEBT' in REDM BANO TQDF LOEV"
     *
     * What a stupid mistake. I was leaving the starting cell marked as visited
     * so if the next word needed that cell it would fail. Now fixed!
     */
    @Test
    public void specialTest() {
        DiceTray dt = new DiceTray(reTray);
        assertTrue(dt.stringFound("END"));
        assertTrue(dt.stringFound("DEBT"));

        System.out.println(dt.toString());
    }

    /**
     * Coverage test. A test just to ensure 100% code coverage since I have some
     * code ready for phase 2.
     */
    @Test
    public void coverageTest() {
        DiceTray dt = new DiceTray();
        assertFalse(dt.stringFound("ZZ"));

        System.out.println(dt.toString());
    }
}
