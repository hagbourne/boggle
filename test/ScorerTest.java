
import static org.junit.Assert.*;
import org.junit.Test;
import java.util.TreeSet;

/**
 * Testing of the Scorer class.
 *
 * @author Keith Talbot
 * @version 12/04/2007
 */
public class ScorerTest {
    //
    // Unshaken, the DiceTray constructor will always contain these dice.
    //
    // L  V  E  S  
    // A  I  O  M  
    // A  X  H  E  
    // Y  Z  N  O  
    //

    /**
     *
     */
    @Test
    public void scorerTest() {
        Scorer scorer = new Scorer();
        DiceTray dt = new DiceTray();
        WordTree wt = new WordTree("res\\bogglewords.txt");
        TreeSet<String> testWords = new TreeSet<>();

        testWords.add("MOVE");
        scorer.score(testWords, dt, wt);
        System.out.println(scorer.toString());
        assertEquals(1, scorer.getTotal());

        testWords.add("VOILA");
        scorer.score(testWords, dt, wt);
        System.out.println(scorer.toString());
        assertEquals(3, scorer.getTotal());

        testWords.add("AXIOMS");
        scorer.score(testWords, dt, wt);
        System.out.println(scorer.toString());
        assertEquals(6, scorer.getTotal());

        testWords.add("A");
        scorer.score(testWords, dt, wt);
        System.out.println(scorer.toString());
        assertEquals(6, scorer.getTotal());

        testWords.add("HEMISPHERE");
        scorer.score(testWords, dt, wt);
        System.out.println(scorer.toString());
        assertEquals(-5, scorer.getTotal());

        testWords.add("LIVEOHMS");
        scorer.score(testWords, dt, wt);
        System.out.println(scorer.toString());
        assertEquals(-16, scorer.getTotal());

        scorer.score(testWords);
        System.out.println(scorer.toString());
        assertEquals(28, scorer.getTotal());

        scorer.score("MOVE VOILA AXIOMS", dt, wt);
        System.out.println(scorer.toString());
        assertEquals(6, scorer.getTotal());
    }
}
