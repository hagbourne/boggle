
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Testing of the WordTree class.
 *
 * @author Keith Talbot
 * @version 12/04/2007
 */
public class WordTreeTest {

    /**
     * Try to find a variety of words, word stems and dead ends.
     */
    @Test
    public void fullTest() {
        WordTree wt = new WordTree("res\\bogglewords.txt");

        // Words & words that are also word stems.
        assertEquals(8, wt.findWord("aardvark"));
        assertEquals(9, wt.findWord("aardvarks"));
        assertEquals(3, wt.findWord("abs"));

        assertEquals(15, wt.findWord("invariabilities"));
        assertEquals(15, wt.findWord("leisurelinesses"));
        assertEquals(16, wt.findWord("rationalizations"));

        assertEquals(3, wt.findWord("zoo"));
        assertEquals(9, wt.findWord("zymurgies"));
        assertEquals(7, wt.findWord("zymurgy"));

        assertEquals(8, wt.findWord("aardvark"));

        // Words stems.
        assertEquals(-1, wt.findWord("a"));
        assertEquals(-2, wt.findWord("aa"));
        assertEquals(-4, wt.findWord("aard"));
        assertEquals(-7, wt.findWord("aardvar"));

        assertEquals(-1, wt.findWord("g"));
        assertEquals(-3, wt.findWord("guf"));
        assertEquals(-5, wt.findWord("guffa"));
        assertEquals(-3, wt.findWord("gui"));

        assertEquals(-7, wt.findWord("zucchin"));
        assertEquals(-4, wt.findWord("zucc"));
        assertEquals(-2, wt.findWord("zu"));
        assertEquals(-1, wt.findWord("z"));

        // Dead ends.
        assertEquals(0, wt.findWord("aaa"));
        assertEquals(0, wt.findWord("eh"));
        assertEquals(0, wt.findWord("qt"));
        assertEquals(0, wt.findWord("qv"));
        assertEquals(0, wt.findWord("missional"));
        assertEquals(0, wt.findWord("missiom"));
        assertEquals(0, wt.findWord("zx"));
    }
}
